/***********************************************************
*** Instituto Superior Tecnico: AAC, 2012/2013.
*** Project 3.
*** 
*** ______________Histogram Equalization___________________
*** 
*** Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
*** Tomas Ferreirinha, 67725. tomas619@hotmail.com
***
*** Program takes a P5 pgm image file as input and ouputs the
*** equalized image also in P5 pgm format.
*** Max graylevels supported = 256. pgm file comments are 
*** not supported.

*** The version that yields the best speedup is:
*** 
*** >>>> HistogramEqualization_s2_p_unr <<<<<
***
***
***********************************************************/

    /*Included Libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "immintrin.h"
#include "emmintrin.h"
#include "xmmintrin.h"
#include "papi.h"

    /*Program Definitions*/
#define NEW_GRAYS 250
#define MAXLINE 4096
#define MAXFILENAME 1024

    /*Type definitions*/
typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;

/*********************************************
* The version that yields the best speedup is:
* 
* >>>> HistogramEqualization_s2_p_unr <<<<<
*
* Functions that will be benchmarked:
*********************************************/

/**** Regular version ****/
void HistogramEqualization_original(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector);

/**** BEST VERSION: i32 version s2: Section 2 with ia32 & multiplication moved from section 3 to section 2 & LoopUnrroling and Software Pipelining *****/
void HistogramEqualization_s2_p_unr(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector);

/****  i32 version s1s3: Sections 1 and 3 with ia32 *****/
void HistogramEqualization_s1s3(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector);

/****  i32 version s2_p: Section 2 with ia32 & multiplication moved from section 3 to section 2. *****/
void HistogramEqualization_s2_p(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector);

/****  i32 version s1: Section 1 with ia32 *****/
void HistogramEqualization_s1(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector);

FILE* createfiledot(char* infilename,char* extension, int flag);

int main(int argc,char **argv){

    u8*img;
    u8*finalimgHw;
    u8*finalimg_original;

	FILE *fp_pgm_original;
	FILE *fp_pgm__s1s3;
    FILE *fp_pgm__original;
    char lbuffer[MAXLINE];
	char sbuffer[MAXLINE]; 
	char cbuffer;
    int width,height,area,gray_levels,i;
    int retval;
    
	if(argc < 2){
		printf("[ERROR] Usage: htool <input_image.pgm>\n");
		exit(-1);
	}
/*-----------------Read Image Header------------------*/

	if((fp_pgm_original = fopen(argv[1],"r"))==NULL){
		printf("[ERROR] Input file not found\n");
		exit(-1);
	}
	fgets(lbuffer, MAXLINE, fp_pgm_original); /*Read and verify Version*/
	sscanf(lbuffer,"%s",sbuffer);
	if(strcmp(sbuffer,"P5")!=0){
		printf("[ERROR] Input file is not pgm version P5\n");
		exit(-1);
	}
	fgets(lbuffer, MAXLINE, fp_pgm_original); /*Read and verify image size*/
	sscanf(lbuffer,"%d %d",&width,&height);
	if(width<=0 || height<=0){
		printf("[ERROR]: image width and height must be non negative 32bit integer numbers\n");
		exit(-1);
	}
	fgets(lbuffer, MAXLINE, fp_pgm_original); /*Read and verify number of gray levels*/
	sscanf(lbuffer,"%d",&gray_levels);
	if(gray_levels<=0 || gray_levels>255){
		printf("[ERROR]: Max number of gray levels supported is 255\n");
		exit(-1);
	}
    
/*-----------------Allocate Image Buffer--------------*/

    area = width*height;
    img = (u8*)malloc(area*sizeof(u8));
    if(img == NULL){
        printf("[ERROR]: Memory Allocation Error\n");
		exit(-1);
    }
    /*Create Duplicates*/
    finalimgHw = (u8*)malloc(area*sizeof(u8));
    if(finalimgHw == NULL){
        printf("[ERROR]: Memory Allocation Error\n");
		exit(-1);
    }
    finalimg_original = (u8*)malloc(area*sizeof(u8));
    if(finalimg_original == NULL){
        printf("[ERROR]: Memory Allocation Error\n");
		exit(-1);
    }

/*-----------------Read Image from File---------------*/

    i=0;
    while(!feof(fp_pgm_original) && i < area){	
		cbuffer = fgetc(fp_pgm_original);
        img[i] = cbuffer;

		i++;
	}

/*-----------------Equalize Image---------------*/


    printf("Initializing PAPI library to get number of clock cycles...\n\n");
	if((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT )
	{
	  printf("Library initialization error! \n");
	  exit(1);
	}
    srand(time(NULL));
    printf("Image Area: %d\n",area);

    printf("\n===========================================================\n");
	printf("              COMPUTING HISTOGRAM EQUALIZATION               \n");
    printf("===========================================================\n");
    
    double timeVector_original;
    double timeVector_s1s3;
    double timeVector_s1;
    double timeVector_s2_p;
    double timeVector_s2_p_unr;

    /*This version writes into finalimg_original and will later output <filename>_s.pgm*/
    for(i=0;i<2;i++)    
    	HistogramEqualization_original(img,finalimg_original,area,gray_levels,&timeVector_original);

    /*The last one of this versions writes into finalimgHw and will later output <filename>_h.pgm*/
    for(i=0;i<2;i++)
    	HistogramEqualization_s1s3(img,finalimgHw,area,gray_levels,&timeVector_s1s3);
              
    for(i=0;i<2;i++)
    	HistogramEqualization_s1(img,finalimgHw,area,gray_levels,&timeVector_s1);
        
    for(i=0;i<2;i++)
    	HistogramEqualization_s2_p(img,finalimgHw,area,gray_levels,&timeVector_s2_p);
        
    for(i=0;i<2;i++)
    	HistogramEqualization_s2_p_unr(img,finalimgHw,area,gray_levels,&timeVector_s2_p_unr);
        
    printf("+---------------+--------------------+--------------------+\n");
    printf("+    version    +    time(us)        +speedup(vs original)+\n");
    printf("+---------------+--------------------+--------------------+\n");
    printf("+   original    +%14.6lf      +%16.4lf    +\n",timeVector_original,timeVector_original/timeVector_original);
    printf("+   s1s3        +%14.6lf      +%16.4lf    +\n",timeVector_s1s3,timeVector_original/timeVector_s1s3);
    printf("+   s1          +%14.6lf      +%16.4lf    +\n",timeVector_s1,timeVector_original/timeVector_s1);
    printf("+   s2_p        +%14.6lf      +%16.4lf    +\n",timeVector_s2_p,timeVector_original/timeVector_s2_p);
    printf("+   s2_p_unr    +%14.6lf      +%16.4lf    +\n",timeVector_s2_p_unr,timeVector_original/timeVector_s2_p_unr);
    printf("+---------------+--------------------+--------------------+\n");

    
/*-----------------Write Results to File---------------*/
    printf("\nWriting results to file....\n");

    if((fp_pgm__s1s3 = createfiledot(argv[1],".pgm",0))==NULL){
        printf("[ERROR] Could not create output file\n");
        exit(-1);
    }
    if((fp_pgm__original = createfiledot(argv[1],".pgm",1))==NULL){
        printf("[ERROR] Could not create output file\n");
        exit(-1);
    }
    
	fprintf(fp_pgm__s1s3,"P5\n");
	fprintf(fp_pgm__s1s3,"%d %d\n",width,height);
	fprintf(fp_pgm__s1s3,"%d\n",gray_levels);
    
    fprintf(fp_pgm__original,"P5\n");
	fprintf(fp_pgm__original,"%d %d\n",width,height);
	fprintf(fp_pgm__original,"%d\n",gray_levels);
    
    i=0;
    while(i<area){	
		fprintf(fp_pgm__s1s3,"%c",finalimgHw[i]);
		i++;
	}
    i=0;
    while(i<area){	
		fprintf(fp_pgm__original,"%c",finalimg_original[i]);
		i++;
	}
    fclose(fp_pgm_original);
	fclose(fp_pgm__s1s3);
    fclose(fp_pgm__original);
    return 0;
}

FILE* createfiledot(char* infilename,char* extension,int flag){
	int i = 0;
	char outfilename[MAXFILENAME];	
	FILE * fp_out;

	if(((int)strlen(infilename)-3) < 0 || (int)strlen(infilename) > MAXFILENAME) return NULL;

	for(i=0;i<((int)strlen(infilename)-4);i++){
		outfilename[i] = infilename[i];
	}
	
    if(flag==0){
        outfilename[strlen(infilename)-4] = '_';
        outfilename[strlen(infilename)-3] = 'h';
        outfilename[strlen(infilename)-2] = '\0';
    }else if(flag==1){
        outfilename[strlen(infilename)-4] = '_';
        outfilename[strlen(infilename)-3] = 's';
        outfilename[strlen(infilename)-2] = '\0';
    }else if(flag==2){
        outfilename[strlen(infilename)-4] = '_';
        outfilename[strlen(infilename)-3] = 'm';
        outfilename[strlen(infilename)-2] = '\0';
    }

	if(strcat(outfilename,extension)==NULL) return NULL;

	fp_out = fopen(outfilename,"w");
	
	return fp_out;
}
/****  i32 version s1s3: Sections 1 and 3 with ia32 *****/
void HistogramEqualization_s1s3(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector){ 

    long long tStart, tEnd;
    u8 grayval;
    int sum,i,j;
    float equal_const;
    int zero[8] = {0,0,0,0,0,0,0,0};
    u32 sum_of_h[256];
    u32 histogram[256];
    u32 histogram0[256];
    u32 histogram1[256];
    u32 histogram2[256];
    u32 histogram3[256];
    u32 histogram4[256];
    u32 histogram5[256];
    u32 histogram6[256];
    u32 histogram7[256];
    u32 g0,g1,g2,g3,g4,g5,g6,g7;
    u32 s0,s1,s2,s3,s4,s5,s6,s7;
    u32 part_0,part_1;
    
/*************************** START BENCHMARK ***************************/
    tStart = PAPI_get_real_usec();
    __m256i auxBuff;
    __m128i mm_grays;
    
    __m128i a0_mm128i;
    __m128i a1_mm128i;
    __m128i b_mm128i;
    __m128i r0_mm128i;
    __m128i r1_mm128i;
    
    /*Reset Histogram*/
    auxBuff = _mm256_loadu_si256 ((__m256i *)(((int*)(zero))));   
    for(i=0;i<=gray_levels;i+=8){
	_mm256_storeu_si256 ((__m256i *) (histogram0+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram1+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram2+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram3+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram4+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram5+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram6+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram7+i),auxBuff);
    }
    
    /*********** SECTION 1 *************/
    
    /*Prepare a _m256i vector with 1's*/         
    b_mm128i = _mm_set1_epi32 (1);
    
    int cnt=0;
    for(i=0;i<area;i+=8){
    
        /*grayval = img[i];
        histogram[grayval] = histogram[grayval] + 1;*/

        /*Collect 8 grayvalues*/      
        mm_grays = _mm_loadu_si128 ((__m128i*)(((char*) img)+i));
        g0=_mm_extract_epi8 (mm_grays,0);
        g1=_mm_extract_epi8 (mm_grays,1);
        g2=_mm_extract_epi8 (mm_grays,2);
        g3=_mm_extract_epi8 (mm_grays,3);
        g4=_mm_extract_epi8 (mm_grays,4);
        g5=_mm_extract_epi8 (mm_grays,5);
        g6=_mm_extract_epi8 (mm_grays,6);
        g7=_mm_extract_epi8 (mm_grays,7);
        
        a0_mm128i =_mm_set_epi32(   histogram0[g0],
                                    histogram1[g1],
                                    histogram2[g2],
                                    histogram3[g3]);
                                    
        a1_mm128i =_mm_set_epi32(   histogram4[g4],
                                    histogram5[g5],
                                    histogram6[g6],
                                    histogram7[g7]);                            
       
        r0_mm128i = _mm_add_epi32 (a0_mm128i,b_mm128i);
        r1_mm128i = _mm_add_epi32 (a1_mm128i,b_mm128i);
        
        histogram0[g0]=_mm_extract_epi32 (r0_mm128i, 0);
        histogram1[g1]=_mm_extract_epi32 (r0_mm128i, 1);
        histogram2[g2]=_mm_extract_epi32 (r0_mm128i, 2);
        histogram3[g3]=_mm_extract_epi32 (r0_mm128i, 3);
        
        histogram4[g4]=_mm_extract_epi32 (r1_mm128i, 0);
        histogram5[g5]=_mm_extract_epi32 (r1_mm128i, 1);
        histogram6[g6]=_mm_extract_epi32 (r1_mm128i, 2);
        histogram7[g7]=_mm_extract_epi32 (r1_mm128i, 3);
        
    }    
    __m128i vmm0;
    __m128i vmm1;
    __m128i vmm2;
    __m128i vmm3;
    __m128i vmm4;
    __m128i vmm5;
    __m128i vmm6;
    __m128i vmm7;
    
    __m128i mm_res01; 
    __m128i mm_res23; 
    __m128i mm_res45; 
    __m128i mm_res67; 
    __m128i mm_resUP; 
    __m128i mm_resDN; 
    __m128i mm_res; 
        
    /*Add the eight partial histogram vectors*/
    for(i=0;i<=gray_levels;i+=4){
                        
        vmm0 = _mm_loadu_si128 ((__m128i*)(((int*) histogram0)+i));
        vmm1 = _mm_loadu_si128 ((__m128i*)(((int*) histogram1)+i));
        vmm2 = _mm_loadu_si128 ((__m128i*)(((int*) histogram2)+i));
        vmm3 = _mm_loadu_si128 ((__m128i*)(((int*) histogram3)+i));
        
        vmm4 = _mm_loadu_si128 ((__m128i*)(((int*) histogram4)+i));
        vmm5 = _mm_loadu_si128 ((__m128i*)(((int*) histogram5)+i));
        vmm6 = _mm_loadu_si128 ((__m128i*)(((int*) histogram6)+i));
        vmm7 = _mm_loadu_si128 ((__m128i*)(((int*) histogram7)+i));
         
        
        mm_res01 = _mm_add_epi32 (vmm0, vmm1);
        mm_res23 = _mm_add_epi32 (vmm2, vmm3);
        mm_res45 = _mm_add_epi32 (vmm4, vmm5);
        mm_res67 = _mm_add_epi32 (vmm6, vmm7); 
        
        mm_resUP = _mm_add_epi32 (mm_res01, mm_res23);
        mm_resDN = _mm_add_epi32 (mm_res45, mm_res67);
        
        mm_res = _mm_add_epi32 (mm_resUP, mm_resDN);

        histogram[i]    =  _mm_extract_epi32 (mm_res, 0);
        histogram[i+1]  =  _mm_extract_epi32 (mm_res, 1);
        histogram[i+2]  =  _mm_extract_epi32 (mm_res, 2);
        histogram[i+3]  =  _mm_extract_epi32 (mm_res, 3);
    
    }
    
    /*********** SECTION 2 *************/
    /*Compute Sum of Histogram and Equalization constant*/
    sum=0;
    for(i=0; i<gray_levels;i++){
        sum = sum + histogram[i];
        sum_of_h[i] = sum;
    }
    equal_const = (float)NEW_GRAYS/(float)area;
    
    /*********** SECTION 3 *************/
    
    /*Compute Equalized Image*/
    
    __m128 vect_a;
    __m128 vect_b;
    __m128 vect_res;
    float ptr_res[8];
    
    /*load equal const into the 8positions of vect b*/
    vect_b = _mm_set1_ps(equal_const);
    
    for(i=0;i<area;i+=4){
    
        /*grayval = img[i];
        img[i] = sum_of_h[grayval]*equal_const;*/
    
        s0 = sum_of_h[img[i]];
        s1 = sum_of_h[img[i+1]];
        s2 = sum_of_h[img[i+2]];
        s3 = sum_of_h[img[i+3]];
           
        vect_a=_mm_set_ps ((float)s3, 
                           (float)s2, 
                           (float)s1, 
                           (float)s0); 
        
        vect_res= _mm_mul_ps (vect_b, vect_a);
        
        _mm_storeu_ps (ptr_res, vect_res);

        finalimg[i]   = ptr_res[0];
        finalimg[i+1] = ptr_res[1];
        finalimg[i+2] = ptr_res[2];
        finalimg[i+3] = ptr_res[3];
    }
    
    tEnd = PAPI_get_real_usec();

    *timeVector = ((double)(tEnd - tStart));

/*************************** END BENCHMARK *****************************/
}
/****  i32 version s2_p: Section 2 with ia32 & multiplication moved from section 3 to section 2. *****/
void HistogramEqualization_s2_p(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector){ 

    long long tStart, tEnd;
    u8 grayval;
    int i,j;
    float equal_const;
    int zero[8] = {0,0,0,0,0,0,0,0};
    float sum_of_h[256];
    u32 histogram[256];
    u32 g0,g1,g2,g3,g4,g5,g6,g7;
    u32 s0,s1,s2,s3,s4,s5,s6,s7;
    u32 part_0,part_1;
        
/*************************** START BENCHMARK ***************************/
    tStart = PAPI_get_real_usec();
    
    __m256i auxBuff;
    __m128i mm_grays;
    
    __m128i a0_mm128i;
    __m128i a1_mm128i;
    __m128i b_mm128i;
    __m128i r0_mm128i;
    __m128i r1_mm128i;
    
    /*Reset Histogram*/
    auxBuff = _mm256_loadu_si256 ((__m256i *)(((int*)(zero))));   
    for(i=0;i<=gray_levels;i+=8){
	_mm256_storeu_si256 ((__m256i *) (histogram+i),auxBuff);
    }
    
    /*********** SECTION 1 *************/
    
    /*Prepare a _m256i vector with 1's*/         
    b_mm128i = _mm_set1_epi32 (1);
    
    int cnt=0;
    for(i=0;i<area;i++){
        grayval = img[i];
        histogram[grayval] = histogram[grayval] + 1;
    }
    /*********** SECTION 2 *************/
    /*Compute Sum of Histogram and Equalization constant*/

    __m128 vect_b;
    __m128 mm_soh;
    __m128 mm_soh_mult;
    
        /*load equal const into the 8positions of vect b*/
    equal_const = (float)NEW_GRAYS/(float)area;
    vect_b = _mm_set1_ps(equal_const);

    int sum0=0;
    int sum1=0;
    int sum2=0;
    int sum3=0;
    for(i=0; i<=gray_levels;i+=4){
    
        sum0 = sum0 + histogram[i];
        sum1 = sum0 + histogram[i+1];
        sum2 = sum1 + histogram[i+2];
        sum3 = sum2 + histogram[i+3];

        sum_of_h[i] = sum0;
        sum_of_h[i+1] = sum1;
        sum_of_h[i+2] = sum2;
        sum_of_h[i+3] = sum3;
        
        mm_soh = _mm_loadu_ps ((((float*)sum_of_h)+i));

        mm_soh_mult = _mm_mul_ps(vect_b, mm_soh);
        
        _mm_storeu_ps ((((float*) sum_of_h)+i), mm_soh_mult);
        
        sum0 = sum3;
    }
    
    /*********** SECTION 3 *************/
    
    /*Compute Equalized Image*/
    
    __m128 vect_a;
    __m128 vect_res;
    float ptr_res[8];
    
    for(i=0;i<area;i+=4){
    
        /*grayval = img[i];
        img[i] = sum_of_h[grayval]*equal_const;*/
    
        finalimg[i]   = sum_of_h[img[i]];
        finalimg[i+1] = sum_of_h[img[i+1]];
        finalimg[i+2] = sum_of_h[img[i+2]];
        finalimg[i+3] = sum_of_h[img[i+3]];
    }
    
    tEnd = PAPI_get_real_usec();

    *timeVector = ((double)(tEnd - tStart));

/*************************** END BENCHMARK *****************************/   
}
/****  i32 version s1: Section 1 with ia32 *****/
void HistogramEqualization_s1(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector){ 

    long long tStart, tEnd;
    u8 grayval;
    int sum,i,j;
    float equal_const;
    int zero[8] = {0,0,0,0,0,0,0,0};
    u32 sum_of_h[256];
    u32 histogram[256];
    u32 histogram0[256];
    u32 histogram1[256];
    u32 histogram2[256];
    u32 histogram3[256];
    u32 histogram4[256];
    u32 histogram5[256];
    u32 histogram6[256];
    u32 histogram7[256];
    u32 g0,g1,g2,g3,g4,g5,g6,g7;
    u32 s0,s1,s2,s3,s4,s5,s6,s7;
    u32 part_0,part_1;
    
/*************************** START BENCHMARK ***************************/
    tStart = PAPI_get_real_usec();
    __m256i auxBuff;
    __m128i mm_grays;
    
    __m128i a0_mm128i;
    __m128i a1_mm128i;
    __m128i b_mm128i;
    __m128i r0_mm128i;
    __m128i r1_mm128i;
    
    /*Reset Histogram*/
    auxBuff = _mm256_loadu_si256 ((__m256i *)(((int*)(zero))));   
    for(i=0;i<=gray_levels;i+=8){
	_mm256_storeu_si256 ((__m256i *) (histogram0+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram1+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram2+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram3+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram4+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram5+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram6+i),auxBuff);
	_mm256_storeu_si256 ((__m256i *) (histogram7+i),auxBuff);
    }
    
    /*********** SECTION 1 *************/
    
    /*Prepare a _m256i vector with 1's*/         
    b_mm128i = _mm_set1_epi32 (1);
    
    int cnt=0;
    for(i=0;i<area;i+=8){
    
        /*grayval = img[i];
        histogram[grayval] = histogram[grayval] + 1;*/

        /*Collect 8 grayvalues*/      
        mm_grays = _mm_loadu_si128 ((__m128i*)(((char*) img)+i));
        g0=_mm_extract_epi8 (mm_grays,0);
        g1=_mm_extract_epi8 (mm_grays,1);
        g2=_mm_extract_epi8 (mm_grays,2);
        g3=_mm_extract_epi8 (mm_grays,3);
        g4=_mm_extract_epi8 (mm_grays,4);
        g5=_mm_extract_epi8 (mm_grays,5);
        g6=_mm_extract_epi8 (mm_grays,6);
        g7=_mm_extract_epi8 (mm_grays,7);
        
        a0_mm128i =_mm_set_epi32(   histogram0[g0],
                                    histogram1[g1],
                                    histogram2[g2],
                                    histogram3[g3]);
                                    
        a1_mm128i =_mm_set_epi32(   histogram4[g4],
                                    histogram5[g5],
                                    histogram6[g6],
                                    histogram7[g7]);                            
       
        r0_mm128i = _mm_add_epi32 (a0_mm128i,b_mm128i);
        r1_mm128i = _mm_add_epi32 (a1_mm128i,b_mm128i);
        
        histogram0[g0]=_mm_extract_epi32 (r0_mm128i, 0);
        histogram1[g1]=_mm_extract_epi32 (r0_mm128i, 1);
        histogram2[g2]=_mm_extract_epi32 (r0_mm128i, 2);
        histogram3[g3]=_mm_extract_epi32 (r0_mm128i, 3);
        
        histogram4[g4]=_mm_extract_epi32 (r1_mm128i, 0);
        histogram5[g5]=_mm_extract_epi32 (r1_mm128i, 1);
        histogram6[g6]=_mm_extract_epi32 (r1_mm128i, 2);
        histogram7[g7]=_mm_extract_epi32 (r1_mm128i, 3);
        
    }    
    __m128i vmm0;
    __m128i vmm1;
    __m128i vmm2;
    __m128i vmm3;
    __m128i vmm4;
    __m128i vmm5;
    __m128i vmm6;
    __m128i vmm7;
    
    __m128i mm_res01; 
    __m128i mm_res23; 
    __m128i mm_res45; 
    __m128i mm_res67; 
    __m128i mm_resUP; 
    __m128i mm_resDN; 
    __m128i mm_res; 
        
    /*Add the eight partial histogram vectors*/
    for(i=0;i<=gray_levels;i+=4){
                        
        vmm0 = _mm_loadu_si128 ((__m128i*)(((int*) histogram0)+i));
        vmm1 = _mm_loadu_si128 ((__m128i*)(((int*) histogram1)+i));
        vmm2 = _mm_loadu_si128 ((__m128i*)(((int*) histogram2)+i));
        vmm3 = _mm_loadu_si128 ((__m128i*)(((int*) histogram3)+i));
        
        vmm4 = _mm_loadu_si128 ((__m128i*)(((int*) histogram4)+i));
        vmm5 = _mm_loadu_si128 ((__m128i*)(((int*) histogram5)+i));
        vmm6 = _mm_loadu_si128 ((__m128i*)(((int*) histogram6)+i));
        vmm7 = _mm_loadu_si128 ((__m128i*)(((int*) histogram7)+i));
         
        
        mm_res01 = _mm_add_epi32 (vmm0, vmm1);
        mm_res23 = _mm_add_epi32 (vmm2, vmm3);
        mm_res45 = _mm_add_epi32 (vmm4, vmm5);
        mm_res67 = _mm_add_epi32 (vmm6, vmm7); 
        
        mm_resUP = _mm_add_epi32 (mm_res01, mm_res23);
        mm_resDN = _mm_add_epi32 (mm_res45, mm_res67);
        
        mm_res = _mm_add_epi32 (mm_resUP, mm_resDN);

        histogram[i]    =  _mm_extract_epi32 (mm_res, 0);
        histogram[i+1]  =  _mm_extract_epi32 (mm_res, 1);
        histogram[i+2]  =  _mm_extract_epi32 (mm_res, 2);
        histogram[i+3]  =  _mm_extract_epi32 (mm_res, 3);
    
    }
    
    /*********** SECTION 2 *************/
    /*Compute Sum of Histogram and Equalization constant*/
    sum=0;
    for(i=0; i<gray_levels;i++){
        sum = sum + histogram[i];
        sum_of_h[i] = sum;
    }
    equal_const = (float)NEW_GRAYS/(float)area;
    
    /*********** SECTION 3 *************/
    
    /*Compute Equalized Image*/
    
    __m128 vect_a;
    __m128 vect_b;
    __m128 vect_res;
    float ptr_res[8];
    
    /*load equal const into the 8positions of vect b*/
    vect_b = _mm_set1_ps(equal_const);
    
    for(i=0;i<area;i++){
        grayval = img[i];
        finalimg[i] = sum_of_h[grayval]*equal_const;
    }
    
    tEnd = PAPI_get_real_usec();

    *timeVector = ((double)(tEnd - tStart));

/*************************** END BENCHMARK *****************************/ 
}

/****  i32 version s2: Section 2 with ia32 & multiplication moved from section 3 to section 2 & LoopUnrroling and Software Pipelining *****/
void HistogramEqualization_s2_p_unr(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector){ 

    long long tStart, tEnd;
    u8 grayval0, grayval1, grayval2, grayval3, grayval4, grayval5, grayval6, grayval7, grayval8, grayval9;
    u8 grayval10, grayval11, grayval12, grayval13, grayval14, grayval15, grayval16, grayval17, grayval18, grayval19;

    int i,j;
    float equal_const;
    int zero[8] = {0,0,0,0,0,0,0,0};
    float sum_of_h[256];
    u32 histogram[256];
    u32 g0,g1,g2,g3,g4,g5,g6,g7;
    u32 s0,s1,s2,s3,s4,s5,s6,s7;
    u32 part_0,part_1;
        
/*************************** START BENCHMARK ***************************/
    tStart = PAPI_get_real_usec();
    
    __m256i auxBuff;
    __m128i mm_grays;
    
    __m128i a0_mm128i;
    __m128i a1_mm128i;
    __m128i b_mm128i;
    __m128i r0_mm128i;
    __m128i r1_mm128i;
    
    /*Reset Histogram*/
    auxBuff = _mm256_loadu_si256 ((__m256i *)(((int*)(zero))));   
    for(i=0;i<=gray_levels;i+=8){
	_mm256_storeu_si256 ((__m256i *) (histogram+i),auxBuff);
    }
    
    /*********** SECTION 1 *************/
    
    /*Prepare a _m256i vector with 1's*/         
    b_mm128i = _mm_set1_epi32 (1);
    
    int cnt=0;
    for(i=0;i<area-19;i+=20){
        grayval0 = img[i];
        grayval1 = img[i+1];
        grayval2 = img[i+2];
        grayval3 = img[i+3];
        grayval4 = img[i+4];
        grayval5 = img[i+5];
        grayval6 = img[i+6];
        grayval7 = img[i+7];
        grayval8 = img[i+8];
        grayval9 = img[i+9];
        grayval10 = img[i+10];
        grayval11 = img[i+11];
        grayval12 = img[i+12];
        grayval13 = img[i+13];
        grayval14 = img[i+14];
        grayval15 = img[i+15];
        grayval16 = img[i+16];
        grayval17 = img[i+17];
        grayval18 = img[i+18];
        grayval19 = img[i+19];

        histogram[grayval0] = histogram[grayval0] + 1;
        histogram[grayval1] = histogram[grayval1] + 1;
        histogram[grayval2] = histogram[grayval2] + 1;
        histogram[grayval3] = histogram[grayval3] + 1;
        histogram[grayval4] = histogram[grayval4] + 1;
        histogram[grayval5] = histogram[grayval5] + 1;
        histogram[grayval6] = histogram[grayval6] + 1;
        histogram[grayval7] = histogram[grayval7] + 1;
        histogram[grayval8] = histogram[grayval8] + 1;
        histogram[grayval9] = histogram[grayval9] + 1;
        histogram[grayval10] = histogram[grayval10] + 1;
        histogram[grayval11] = histogram[grayval11] + 1;
        histogram[grayval12] = histogram[grayval12] + 1;
        histogram[grayval13] = histogram[grayval13] + 1;
        histogram[grayval14] = histogram[grayval14] + 1;
        histogram[grayval15] = histogram[grayval15] + 1;
        histogram[grayval16] = histogram[grayval16] + 1;
        histogram[grayval17] = histogram[grayval17] + 1;
        histogram[grayval18] = histogram[grayval18] + 1;
        histogram[grayval19] = histogram[grayval19] + 1;
    }
    for(;i<area;i++){ //Epilogue
        grayval0 = img[i];
        histogram[grayval0] = histogram[grayval0] + 1;
    }
	
    /*********** SECTION 2 *************/
    /*Compute Sum of Histogram and Equalization constant*/
    __m128 vect_b;
    __m128 mm_soh;
    __m128 mm_soh_mult;
    
        /*load equal const into the 8positions of vect b*/
    equal_const = (float)NEW_GRAYS/(float)area;
    vect_b = _mm_set1_ps(equal_const);
    
    int sum0=0;
    int sum1=0;
    int sum2=0;
    int sum3=0;
    for(i=0; i<=(gray_levels-3);i+=4){
    
        sum0 = sum3 + histogram[i];
        sum1 = sum0 + histogram[i+1];
        sum2 = sum1 + histogram[i+2];
        sum3 = sum2 + histogram[i+3];

        sum_of_h[i] = sum0;
        sum_of_h[i+1] = sum1;
        sum_of_h[i+2] = sum2;
        sum_of_h[i+3] = sum3;
        
        mm_soh = _mm_loadu_ps ((((float*)sum_of_h)+i));

        mm_soh_mult = _mm_mul_ps(vect_b, mm_soh);
        
        _mm_storeu_ps ((((float*) sum_of_h)+i), mm_soh_mult);
        
    }
    for(;i<=gray_levels;i++)
        sum_of_h[i]=(sum_of_h[i-1]+histogram[i])*equal_const;
    
    /*********** SECTION 3 *************/
    
    /*Compute Equalized Image*/
    
    __m128 vect_a;
    __m128 vect_res;
    float ptr_res[8];
    
    for(i=0;i<area-15;i+=16){
    
        /*grayval = img[i];
        img[i] = sum_of_h[grayval]*equal_const;*/
    
        finalimg[i]   = sum_of_h[img[i]];
        finalimg[i+1] = sum_of_h[img[i+1]];
        finalimg[i+2] = sum_of_h[img[i+2]];
        finalimg[i+3] = sum_of_h[img[i+3]];
        finalimg[i+4] = sum_of_h[img[i+4]];
        finalimg[i+5] = sum_of_h[img[i+5]];
        finalimg[i+6] = sum_of_h[img[i+6]];
        finalimg[i+7] = sum_of_h[img[i+7]];
        finalimg[i+8] = sum_of_h[img[i+8]];
        finalimg[i+9] = sum_of_h[img[i+9]];
        finalimg[i+10] = sum_of_h[img[i+10]];
        finalimg[i+11] = sum_of_h[img[i+11]];
        finalimg[i+12] = sum_of_h[img[i+12]];
        finalimg[i+13] = sum_of_h[img[i+13]];
        finalimg[i+14] = sum_of_h[img[i+14]];
        finalimg[i+15] = sum_of_h[img[i+15]];


    }
    for(;i<area;i++) //Epilogue
        finalimg[i]   = sum_of_h[img[i]];

    tEnd = PAPI_get_real_usec();

    *timeVector = ((double)(tEnd - tStart));

/*************************** END BENCHMARK *****************************/
}
/**** Regular version ****/
void HistogramEqualization_original(u8*img,u8*finalimg, int area,int gray_levels, double* timeVector){ 

    long long tStart, tEnd;
    u8 grayval;
    int sum,i;
    float equal_const;
    u32 sum_of_h[256];
    u32 histogram[256];
    
/*************************** START BENCHMARK ***************************/
    
    tStart = PAPI_get_real_usec();
    
/*********** SECTION 1 *************/
/*Compute Histogram*/
    for(i=0;i<gray_levels;i++)
        histogram[i]=0;

    for(i=0;i<area;i++){
        grayval = img[i];
        histogram[grayval] = histogram[grayval] + 1;
    }    
    
/*********** SECTION 2 *************/
/*Compute Sum of Histogram and Equalization constant*/
    sum=0;
    for(i=0; i<gray_levels;i++){
        sum = sum + histogram[i];
        sum_of_h[i] = sum;
    }
    equal_const = (float)NEW_GRAYS/(float)area;
    
/*********** SECTION 3 *************/
/*Compute Equalized Image*/
    for(i=0;i<area;i++){
        grayval = img[i];
        finalimg[i] = sum_of_h[grayval]*equal_const;
    }
    
    tEnd = PAPI_get_real_usec();

    *timeVector = ((double)(tEnd - tStart));
/*************************** END BENCHMARK *****************************/
}