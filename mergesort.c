/***********************************************************
*** Instituto Superior Tecnico: AAC, 2012/2013.
*** Project 3.
*** 
*** _____________________Merge Sort_________________________
*** 
*** Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
*** Tomas Ferreirinha, 67725. tomas619@hotmail.com
***
*** Program takes txt file with an unsorted list; format:
***
*** <list size>
*** <float number1>
*** <float number2>
*** <float number3>
*** <float number4>
*** ...
***
*** The output is a file with the sorted list.
*** The version that yields the best speedup is:
*** 
*** >>>> Vector_bitonic8_MergeSort <<<<<
***
***
***********************************************************/

    /*program includes*/
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include "immintrin.h"
#include "papi.h"
#include "xmmintrin.h"
    /*program definitions and macros*/
#define MAXLINE 1024
#define min(a, b) (((a) < (b)) ? (a) : (b))

/*********************************************
* The version that yields the best speedup is:
* 
* >>>> Vector_bitonic8_MergeSort <<<<<
*
* Versions that will be benchmarked:
*********************************************/

/**--------------------------------------------------------------------------
** Original Merge Sort
***------------------------------------------------------------------------*/

    /** Original Merge Sort**/
    void OriginalMergeSort(float vec[], int vecSize, float* tmp);

    /** Original Merge Function used by the original Merge Sort **/
    void OriginalMerge(float vec[], int iLeft0, int iRight0, int iEnd0, float tmp[]);

/**--------------------------------------------------------------------------
** Merge Sort with optimized vector copy
***------------------------------------------------------------------------*/

    /** Merge Function used by Merge Sort with vector copy optimized **/
    void Merge(float vec[], int iLeft0, int iRight0, int iEnd0, float tmp[]);

    /** Merge Sort with vector copying optimized **/
    void MergeSort(float vec[], int vecSize, float* tmp);
    
/**--------------------------------------------------------------------------
** Vectorized Merge Sort Bitonic 8
***------------------------------------------------------------------------*/

    /** Merge Function used by Vector_bitonic8_Merge**/
    void Vector_bitonic8_MergeSort(float vec[], int iLeft0, int iRight0, int iEnd0,int iLeft1,int iRight1,int iEnd1,int iLeft2,int iRight2,int iEnd2,int iLeft3,int iRight3,int iEnd3,int iLeft4,int iRight4,int iEnd4, int iLeft5,int iRight5,int iEnd5, float tmp[]);
        
    /** Vectorized Merge Sort bitonic8. This function uses the merge function Vector_bitonic8_MergeSort**/
    void Vector_bitonic8_Merge(float vec[], int vecSize, float* tmp);

    /*This version also uses Vector_Merge in stages where block size 8 is not compatible*/
    void Vector_Merge(float vec[], int iLeft0, int iRight0, int iEnd0, float tmp[]);

/**--------------------------------------------------------------------------
** Vectorized Merge Sort Bitonic 16
***------------------------------------------------------------------------*/

    /** Merge Function used by Vector_bitonic16_Merge **/
    void Vector_bitonic16_MergeSort(float vec[], int iLeft0, int iRight0, int iEnd0,int iLeft1,int iRight1,int iEnd1,int iLeft2,int iRight2,int iEnd2,int iLeft3,int iRight3,int iEnd3,int iLeft4,int iRight4,int iEnd4, int iLeft5,int iRight5,int iEnd5, float tmp[]);

    /** Vectorized Merge Sort bitonic16. This function uses the merge function Vector_bitonic16_MergeSort**/
    void Vector_bitonic16_Merge(float vec[], int vecSize, float* tmp);
    
    /*This version also uses Vector_Merge in stages where block size 16 is not compatible*/
    
/**--------------------------------------------------------------------------
** Bubble Sort (n^2)
***------------------------------------------------------------------------*/

    /** Bubble Sort **/
    void BubbleSort(float vec[], int vecSize);

/*Main Function*/
int main(int argc,char **argv){
	FILE *fp_unsorted;
	FILE *fp_sorted;
    char lbuffer[MAXLINE];
    float *array, *array2;
    float *tmp;
    int array_size, i;
    double time_OriginalMerge, time_Merge, time_Vector_bitonic8, time_Vector_bitonic16, time_Bubble;
    long long tStart, tEnd;
    int retval;
    
	if(argc < 2){
		printf("[ERROR] Usage: mergesort <input_file.txt>\n");
		exit(-1);
	}

	if((fp_unsorted = fopen(argv[1],"r"))==NULL){
		printf("[ERROR] Input file not found\n");
		exit(-1);
	}

	if((fp_sorted = fopen("sorted_out.txt","w"))==NULL){
		printf("[ERROR] Could not create output file not found\n");
		exit(-1);
	}
    
    fgets(lbuffer, MAXLINE, fp_unsorted);
	sscanf(lbuffer,"%d",&array_size); /*Read array size*/
    array = (float*)_mm_malloc(array_size*sizeof(float),32);
    array2 = (float*)_mm_malloc(array_size*sizeof(float),32);
    
    if(array == NULL){
        printf("[ERROR]: Memory Allocation Error\n");
		exit(-1);
    }
    
    i=0;
    while(fgets(lbuffer, MAXLINE, fp_unsorted)!=NULL && i<array_size){
        sscanf(lbuffer,"%f",&array[i]); /*Read array size*/
        array2[i]=array[i];
        i++;
    }
    if(i!=array_size){
        printf("[ERROR] Array incomplete\n");
		exit(-1);
	}
    
    tmp = (float*)_mm_malloc(array_size*sizeof(float),32);
    
    for(i=0; i<array_size;i++){
        tmp[i]=(float)0;
    }
    
    if(tmp == NULL){
        printf("[ERROR]: Memory Allocation Error\n");
		exit(-1);
    }
    
    printf("Initializing PAPI library to get number of clock cycles...\n\n");
	if((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT )
	{
	  printf("Library initialization error! \n");
	  exit(1);
	}
    srand(time(NULL));

 	printf("=====================================================================================================\n");
	printf("   COMPUTING Merge Sort Times\n");
 	printf("=====================================================================================================\n");
    
    OriginalMergeSort(array2, array_size, tmp);
    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }
    tStart = PAPI_get_real_usec();
    OriginalMergeSort(array2, array_size, tmp);
    tEnd = PAPI_get_real_usec();
    time_OriginalMerge = (double)(tEnd - tStart);

    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    MergeSort(array2, array_size, tmp);

    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    tStart = PAPI_get_real_usec();
    MergeSort(array2, array_size, tmp);
    tEnd = PAPI_get_real_usec();
    time_Merge = (double)(tEnd - tStart);

    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    Vector_bitonic8_Merge(array2, array_size, tmp);
   
    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    tStart = PAPI_get_real_usec();
    Vector_bitonic8_Merge(array2, array_size, tmp);
    tEnd = PAPI_get_real_usec();
    time_Vector_bitonic8 = (double)(tEnd - tStart);
    
    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    Vector_bitonic16_Merge(array2, array_size, tmp);
   
    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    tStart = PAPI_get_real_usec();
    Vector_bitonic16_Merge(array2, array_size, tmp);
    tEnd = PAPI_get_real_usec();
    time_Vector_bitonic16 = (double)(tEnd - tStart);
    
    /*Bubble Sort*/
    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    BubbleSort(array2, array_size);
   
    for (i = 0; i < array_size; ++i) {
      	array2[i] = array[i];
    }

    tStart = PAPI_get_real_usec();
    BubbleSort(array2, array_size);
    tEnd = PAPI_get_real_usec();
    time_Bubble = (double)(tEnd - tStart);
     
    printf("+---------------+--------------------+--------------------+--------------------+--------------------+\n");
    printf("+    version    +    time(us)        +speedup(vs optimizd)+speedup(vs original)+speedup(vs bubble)  +\n");
    printf("+---------------+--------------------+--------------------+--------------------+--------------------+\n");
    printf("+bubble sort    +%14.1lf      +%16.4lf    +%16.4lf    +%16.4lf    +\n",time_Bubble,time_Merge/time_Bubble,
                                                                                    time_OriginalMerge/time_Bubble,time_Bubble/time_Bubble);
                                                                                    
    printf("+original merge +%14.1lf      +%16.4lf    +%16.4lf    +%16.4lf    +\n",time_OriginalMerge,time_Merge/time_OriginalMerge,
                                                                                    time_OriginalMerge/time_OriginalMerge,time_Bubble/time_OriginalMerge);
                                                                                    
    printf("+optimized merge+%14.1lf      +%16.4lf    +%16.4lf    +%16.4lf    +\n",time_Merge,time_Merge/time_Merge,
                                                                                    time_OriginalMerge/time_Merge,time_Bubble/time_Merge);
                                                                                    
    printf("+bit16_unr      +%14.1lf      +%16.4lf    +%16.4lf    +%16.4lf    +\n",time_Vector_bitonic16,time_Merge/time_Vector_bitonic16,
                                                                                    time_OriginalMerge/time_Vector_bitonic16,time_Bubble/time_Vector_bitonic16);
                                                                                    
    printf("+bit8_unr       +%14.1lf      +%16.4lf    +%16.4lf    +%16.4lf    +\n",time_Vector_bitonic8,time_Merge/time_Vector_bitonic8,
                                                                                    time_OriginalMerge/time_Vector_bitonic8,time_Bubble/time_Vector_bitonic8);
                                                                                    
   
    printf("+---------------+--------------------+--------------------+--------------------+--------------------+\n"); 
     
    for(i=0;i<array_size;i++)
        fprintf(fp_sorted,"%f\n",array2[i]);
        
    fclose(fp_unsorted);
    fclose(fp_sorted);
    _mm_free(array); 
    _mm_free(array2); 
    _mm_free(tmp);
    return 0;
    
}
/** Parallel Merge function used by the Bitonic8/16 Merge functions when block sizes are not compatible **/
void Vector_Merge(float vec[], int iLeft0, int iRight0, int iEnd0, float tmp[]){
    int m = iLeft0;
    int k = iRight0;
    int j;
    __m128 auxBuff;

  /* While there are elements in the left or right lists */
    for (j = iLeft0; m<iRight0-5 && k<iEnd0-5; j++){
      /* If left list head exists and is <= existing right list head */
        if (vec[m] <= vec[k]){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
        j++;
        if (vec[m] <= vec[k]){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
        j++;
        if (vec[m] <= vec[k]){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
        j++;
        if (vec[m] <= vec[k]){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
        j++;
        if (vec[m] <= vec[k]){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }

    }
    for (; m<iRight0 && k<iEnd0; j++){//EPILOGUE
      /* If left list head exists and is <= existing right list head */
        if (vec[m] <= vec[k]){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
    }

    if(m==iRight0){
        while (j < iEnd0){
            if(iEnd0-j > 3){
                auxBuff = _mm_loadu_ps (vec+k);
                _mm_storeu_ps(tmp+j,auxBuff);
                k+=4;
                j+=4;
            }else{
                tmp[j] = vec[k];
                k++;
                j++;
            }
            
        }
    }else{
        while (j < iEnd0){
            if(iEnd0-j > 3){
                auxBuff = _mm_loadu_ps (vec+m);
                _mm_storeu_ps(tmp+j,auxBuff);
                m+=4;
                j+=4;
            }else{
                tmp[j] = vec[m];
                m++;
                j++;
            }
        }
    }
}
/** Merge Function used by Vector_bitonic8_Merge**/
void Vector_bitonic8_MergeSort(float vec[], int iLeft0, int iRight0, int iEnd0,int iLeft1,int iRight1,int iEnd1,int iLeft2,int iRight2,int iEnd2,int iLeft3,int iRight3,int iEnd3,int iLeft4,int iRight4,int iEnd4, int iLeft5,int iRight5,int iEnd5, float tmp[]){
    
    if((iEnd0-iLeft0)==8){ //Blocks have the same size so it is not required to chech iEnd1-iLeft1
        __m128 auxBuffmin0, auxBuffmax0, desc0, asc0, auxBuffmin1, auxBuffmax1, desc1,asc1, auxBuffmin2, auxBuffmax2, desc2,asc2, auxBuffmin3, auxBuffmax3, desc3,asc3, auxBuffmin4, auxBuffmax4, desc4,asc4,auxBuffmin5, auxBuffmax5, desc5,asc5;
        asc0 = _mm_load_ps(vec+iLeft0);
        desc0 = _mm_load_ps(vec+iRight0); 
        asc1 = _mm_load_ps(vec+iLeft1);
        desc1 = _mm_load_ps(vec+iRight1);
        asc2 = _mm_load_ps(vec+iLeft2);
        desc2 = _mm_load_ps(vec+iRight2);
        asc3 = _mm_load_ps(vec+iLeft3);
        desc3 = _mm_load_ps(vec+iRight3);
        asc4 = _mm_load_ps(vec+iLeft4);
        desc4 = _mm_load_ps(vec+iRight4);
        asc5 = _mm_load_ps(vec+iLeft5);
        desc5 = _mm_load_ps(vec+iRight5);
        desc0 = _mm_shuffle_ps(desc0, desc0, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc1 = _mm_shuffle_ps(desc1, desc1, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc2 = _mm_shuffle_ps(desc2, desc2, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc3 = _mm_shuffle_ps(desc3, desc3, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc4 = _mm_shuffle_ps(desc4, desc4, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc5 = _mm_shuffle_ps(desc5, desc5, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence

        auxBuffmin0 = _mm_min_ps(asc0, desc0);
        auxBuffmax0 = _mm_max_ps(asc0, desc0);
        auxBuffmin1 = _mm_min_ps(asc1, desc1);
        auxBuffmax1 = _mm_max_ps(asc1, desc1);
        auxBuffmin2 = _mm_min_ps(asc2, desc2);
        auxBuffmax2 = _mm_max_ps(asc2, desc2);
        auxBuffmin3 = _mm_min_ps(asc3, desc3);
        auxBuffmax3 = _mm_max_ps(asc3, desc3);        
        auxBuffmin4 = _mm_min_ps(asc4, desc4);
        auxBuffmax4 = _mm_max_ps(asc4, desc4); 
        auxBuffmin5 = _mm_min_ps(asc5, desc5);
        auxBuffmax5 = _mm_max_ps(asc5, desc5);   

        //Second lvl ordering (2 bitonic sequences of 4 floats)
        asc0 = _mm_shuffle_ps(auxBuffmin0, auxBuffmax0, _MM_SHUFFLE(1,0,1,0));
        desc0 = _mm_shuffle_ps(auxBuffmin0, auxBuffmax0, _MM_SHUFFLE(3,2,3,2));
        asc1 = _mm_shuffle_ps(auxBuffmin1, auxBuffmax1, _MM_SHUFFLE(1,0,1,0));
        desc1 = _mm_shuffle_ps(auxBuffmin1, auxBuffmax1, _MM_SHUFFLE(3,2,3,2));
        asc2 = _mm_shuffle_ps(auxBuffmin2, auxBuffmax2, _MM_SHUFFLE(1,0,1,0));
        desc2 = _mm_shuffle_ps(auxBuffmin2, auxBuffmax2, _MM_SHUFFLE(3,2,3,2));
        asc3 = _mm_shuffle_ps(auxBuffmin3, auxBuffmax3, _MM_SHUFFLE(1,0,1,0));
        desc3 = _mm_shuffle_ps(auxBuffmin3, auxBuffmax3, _MM_SHUFFLE(3,2,3,2));
        asc4 = _mm_shuffle_ps(auxBuffmin4, auxBuffmax4, _MM_SHUFFLE(1,0,1,0));
        desc4 = _mm_shuffle_ps(auxBuffmin4, auxBuffmax4, _MM_SHUFFLE(3,2,3,2));
        asc5 = _mm_shuffle_ps(auxBuffmin5, auxBuffmax5, _MM_SHUFFLE(1,0,1,0));
        desc5 = _mm_shuffle_ps(auxBuffmin5, auxBuffmax5, _MM_SHUFFLE(3,2,3,2));
        auxBuffmin0 = _mm_min_ps(asc0, desc0);
        auxBuffmax0 = _mm_max_ps(asc0, desc0);
        auxBuffmin1 = _mm_min_ps(asc1, desc1);
        auxBuffmax1 = _mm_max_ps(asc1, desc1);
        auxBuffmin2 = _mm_min_ps(asc2, desc2);
        auxBuffmax2 = _mm_max_ps(asc2, desc2);
        auxBuffmin3 = _mm_min_ps(asc3, desc3);
        auxBuffmax3 = _mm_max_ps(asc3, desc3);
        auxBuffmin4 = _mm_min_ps(asc4, desc4);
        auxBuffmax4 = _mm_max_ps(asc4, desc4);
        auxBuffmin5 = _mm_min_ps(asc5, desc5);
        auxBuffmax5 = _mm_max_ps(asc5, desc5);
        
        //Third lvl ordering (4 bitonic sequences of 2 floats)
        asc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(3,1,2,0));
        desc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(2,0,3,1));
        asc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(3,1,2,0));
        desc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(2,0,3,1));
        asc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(3,1,2,0));
        desc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(2,0,3,1));
        asc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(3,1,2,0));
        desc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(2,0,3,1));
        asc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(3,1,2,0));
        desc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(2,0,3,1));
        asc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(3,1,2,0));
        desc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(2,0,3,1));
        auxBuffmin0 = _mm_min_ps(asc0, desc0);
        auxBuffmax0 = _mm_max_ps(asc0,desc0);
        auxBuffmin1 = _mm_min_ps(asc1, desc1);
        auxBuffmax1 = _mm_max_ps(asc1,desc1);
        auxBuffmin2 = _mm_min_ps(asc2, desc2);
        auxBuffmax2 = _mm_max_ps(asc2,desc2);
        auxBuffmin3 = _mm_min_ps(asc3, desc3);
        auxBuffmax3 = _mm_max_ps(asc3,desc3);
        auxBuffmin4 = _mm_min_ps(asc4, desc4);
        auxBuffmax4 = _mm_max_ps(asc4,desc4);
        auxBuffmin5 = _mm_min_ps(asc5, desc5);
        auxBuffmax5 = _mm_max_ps(asc5,desc5);
        
        //Finally,reorder buffers to store the ordered array
        asc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(2,0,2,0));
        desc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(3,1,3,1));  
        asc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(2,0,2,0));
        desc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(3,1,3,1));
        asc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(2,0,2,0));
        desc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(3,1,3,1));  
        asc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(2,0,2,0));
        desc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(3,1,3,1));    
        asc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(2,0,2,0));
        desc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(3,1,3,1));   
        asc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(2,0,2,0));
        desc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(3,1,3,1)); 
        asc0 = _mm_shuffle_ps(asc0,asc0,_MM_SHUFFLE(3,1,2,0));
        desc0 = _mm_shuffle_ps(desc0,desc0,_MM_SHUFFLE(3,1,2,0));
        asc1 = _mm_shuffle_ps(asc1,asc1,_MM_SHUFFLE(3,1,2,0));
        desc1 = _mm_shuffle_ps(desc1,desc1,_MM_SHUFFLE(3,1,2,0));
        asc2 = _mm_shuffle_ps(asc2,asc2,_MM_SHUFFLE(3,1,2,0));
        desc2 = _mm_shuffle_ps(desc2,desc2,_MM_SHUFFLE(3,1,2,0));
        asc3 = _mm_shuffle_ps(asc3,asc3,_MM_SHUFFLE(3,1,2,0));
        desc3 = _mm_shuffle_ps(desc3,desc3,_MM_SHUFFLE(3,1,2,0));
        asc4 = _mm_shuffle_ps(asc4,asc4,_MM_SHUFFLE(3,1,2,0));
        desc4 = _mm_shuffle_ps(desc4,desc4,_MM_SHUFFLE(3,1,2,0));
        asc5 = _mm_shuffle_ps(asc5,asc5,_MM_SHUFFLE(3,1,2,0));
        desc5 = _mm_shuffle_ps(desc5,desc5,_MM_SHUFFLE(3,1,2,0));

        _mm_store_ps(tmp+iLeft0, asc0);
        _mm_store_ps(tmp+iRight0, desc0);
        _mm_store_ps(tmp+iLeft1, asc1);
        _mm_store_ps(tmp+iRight1, desc1);
        _mm_store_ps(tmp+iLeft2, asc2);
        _mm_store_ps(tmp+iRight2, desc2);
        _mm_store_ps(tmp+iLeft3, asc3);
        _mm_store_ps(tmp+iRight3, desc3);
        _mm_store_ps(tmp+iLeft4, asc4);
        _mm_store_ps(tmp+iRight4, desc4);
        _mm_store_ps(tmp+iLeft5, asc5);
        _mm_store_ps(tmp+iRight5, desc5);       
    }else{
       Vector_Merge(vec, iLeft0, iRight0, iEnd0, tmp);
       Vector_Merge(vec, iLeft1, iRight1, iEnd1, tmp);
       Vector_Merge(vec, iLeft2, iRight2, iEnd2, tmp);
       Vector_Merge(vec, iLeft3, iRight3, iEnd3, tmp);
       Vector_Merge(vec, iLeft4, iRight4, iEnd4, tmp);
       Vector_Merge(vec, iLeft5, iRight5, iEnd5, tmp);
    }
}
/** Parallel Merge Sort bitonic8. This function uses the merge function Vector_bitonic8_MergeSort**/
void Vector_bitonic8_Merge(float vec[], int vecSize, float* tmp){
    int width;
    float * aux;

  /* Each 1-element run in vec[] is already "sorted". */
 
  /* Make successively longer sorted runs of length 2, 4, 8, 16... until whole array is sorted. */
    for (width = 1; width < vecSize; width = 2 * width){
        int i;
        for (i = 0; i < vecSize-10*width; i = i + 12 * width){
             /* Merge two runs: vec[i:i+width-1] and vec[i+width:i+2*width-1] to tmp[] */
             /* or copy vec[i:n-1] to tmp[] ( if(i+width >= n) ) */
             Vector_bitonic8_MergeSort(vec, i, min(i+width, vecSize), min(i+2*width, vecSize),
					     i+2*width, min(i+3*width, vecSize), min(i+4*width, vecSize),
					     i+4*width, min(i+5*width, vecSize), min(i+6*width, vecSize),
					     i+6*width, min(i+7*width, vecSize), min(i+8*width, vecSize),
					     i+8*width, min(i+9*width, vecSize), min(i+10*width, vecSize),
					     i+10*width, min(i+11*width, vecSize), min(i+12*width, vecSize), tmp);

        }
	 for (; i < vecSize; i = i + 2 * width){
             /* Merge two runs: vec[i:i+width-1] and vec[i+width:i+2*width-1] to tmp[] */
             /* or copy vec[i:n-1] to tmp[] ( if(i+width >= n) ) */
             Vector_Merge(vec, i, min(i+width, vecSize), min(i+2*width, vecSize), tmp);
        }


 
      /* Now work array tmp[] is full of runs of length 2*width. */
      /* Copy array tmp[] to array vec[] for next iteration. */

        aux=vec;
        vec=tmp;
        tmp = aux;
       /* Now array vec is full of runs of length 2*width. */
    }
}
/** Merge Function used by Vector_bitonic16_Merge **/
void Vector_bitonic16_MergeSort(float vec[], int iLeft0, int iRight0, int iEnd0,int iLeft1,int iRight1,int iEnd1,int iLeft2,int iRight2,int iEnd2,int iLeft3,int iRight3,int iEnd3,int iLeft4,int iRight4,int iEnd4, int iLeft5,int iRight5,int iEnd5, float tmp[]){
    
    if((iEnd0-iLeft0)==16){ //Blocks have the same size so it is not required to chech iEnd1-iLeft1
        __m256 auxBuffmin0, auxBuffmax0, desc0, asc0, auxBuffmin1, auxBuffmax1, desc1, asc1, auxBuffmin2, auxBuffmax2, desc2, asc2, auxBuffmin3, auxBuffmax3, desc3, asc3, auxBuffmin4, auxBuffmax4, desc4, asc4;
        __m128 aux0_0, aux0_1, aux1_0, aux1_1, aux2_0, aux2_1,aux3_0, aux3_1,aux4_0, aux4_1;
        
        asc0 = _mm256_load_ps(vec+iLeft0);
        asc1 = _mm256_load_ps(vec+iLeft1);
        asc2 = _mm256_load_ps(vec+iLeft2);
        asc3 = _mm256_load_ps(vec+iLeft3);
        asc4 = _mm256_load_ps(vec+iLeft4);
        
        //Revert order of the last 8 floats since they must be in descending order 
        aux0_0 = _mm_load_ps(vec+iRight0);
        aux0_1 = _mm_load_ps(vec+iRight0+4);
        aux1_0 = _mm_load_ps(vec+iRight1);
        aux1_1 = _mm_load_ps(vec+iRight1+4);
        aux2_0 = _mm_load_ps(vec+iRight2);
        aux2_1 = _mm_load_ps(vec+iRight2+4);
        aux3_0 = _mm_load_ps(vec+iRight3);
        aux3_1 = _mm_load_ps(vec+iRight3+4);
        aux4_0 = _mm_load_ps(vec+iRight4);
        aux4_1 = _mm_load_ps(vec+iRight4+4);

        aux0_0 = _mm_shuffle_ps(aux0_0, aux0_0, _MM_SHUFFLE(0,1,2,3)); 
        aux0_1 = _mm_shuffle_ps(aux0_1, aux0_1, _MM_SHUFFLE(0,1,2,3));
        aux1_0 = _mm_shuffle_ps(aux1_0, aux1_0, _MM_SHUFFLE(0,1,2,3));         
        aux1_1 = _mm_shuffle_ps(aux1_1, aux1_1, _MM_SHUFFLE(0,1,2,3)); 
        aux2_0 = _mm_shuffle_ps(aux2_0, aux2_0, _MM_SHUFFLE(0,1,2,3));         
        aux2_1 = _mm_shuffle_ps(aux2_1, aux2_1, _MM_SHUFFLE(0,1,2,3)); 
        aux3_0 = _mm_shuffle_ps(aux3_0, aux3_0, _MM_SHUFFLE(0,1,2,3));         
        aux3_1 = _mm_shuffle_ps(aux3_1, aux3_1, _MM_SHUFFLE(0,1,2,3)); 
        aux4_0 = _mm_shuffle_ps(aux4_0, aux4_0, _MM_SHUFFLE(0,1,2,3));         
        aux4_1 = _mm_shuffle_ps(aux4_1, aux4_1, _MM_SHUFFLE(0,1,2,3)); 

        desc0 = _mm256_insertf128_ps(asc0,aux0_1,0);//asc0 is used just because it is already set
        desc1 = _mm256_insertf128_ps(asc1,aux1_1,0);//asc1 is used just because it is already set
        desc2 = _mm256_insertf128_ps(asc2,aux2_1,0);//asc2 is used just because it is already set
        desc3 = _mm256_insertf128_ps(asc3,aux3_1,0);//asc3 is used just because it is already set
        desc4 = _mm256_insertf128_ps(asc4,aux4_1,0);//asc4 is used just because it is already set

        desc0 = _mm256_insertf128_ps(desc0,aux0_0,1);
        desc1 = _mm256_insertf128_ps(desc1,aux1_0,1);
        desc2 = _mm256_insertf128_ps(desc2,aux2_0,1);
        desc3 = _mm256_insertf128_ps(desc3,aux3_0,1);
        desc4 = _mm256_insertf128_ps(desc4,aux4_0,1);
        
        auxBuffmax0 = _mm256_max_ps(asc0, desc0);
        auxBuffmin0 = _mm256_min_ps(asc0, desc0);
        auxBuffmax1 = _mm256_max_ps(asc1, desc1);
        auxBuffmin1 = _mm256_min_ps(asc1, desc1);
        auxBuffmax2 = _mm256_max_ps(asc2, desc2);
        auxBuffmin2 = _mm256_min_ps(asc2, desc2);
        auxBuffmax3 = _mm256_max_ps(asc3, desc3);
        auxBuffmin3 = _mm256_min_ps(asc3, desc3);
        auxBuffmax4 = _mm256_max_ps(asc4, desc4);
        auxBuffmin4 = _mm256_min_ps(asc4, desc4);
        
        //Second lvl ordering (2 bitonic sequences of 8 floats)
        aux0_0 = _mm256_extractf128_ps(auxBuffmax0, 0);
        aux0_1 = _mm256_extractf128_ps(auxBuffmin0, 1);
        aux1_0 = _mm256_extractf128_ps(auxBuffmax1, 0);
        aux1_1 = _mm256_extractf128_ps(auxBuffmin1, 1);
        aux2_0 = _mm256_extractf128_ps(auxBuffmax2, 0);
        aux2_1 = _mm256_extractf128_ps(auxBuffmin2, 1);
        aux3_0 = _mm256_extractf128_ps(auxBuffmax3, 0);
        aux3_1 = _mm256_extractf128_ps(auxBuffmin3, 1);
        aux4_0 = _mm256_extractf128_ps(auxBuffmax4, 0);
        aux4_1 = _mm256_extractf128_ps(auxBuffmin4, 1);

        asc0 = _mm256_insertf128_ps(auxBuffmin0,aux0_0,1);
        desc0 = _mm256_insertf128_ps(auxBuffmax0,aux0_1,0);
        asc1 = _mm256_insertf128_ps(auxBuffmin1,aux1_0,1);
        desc1 = _mm256_insertf128_ps(auxBuffmax1,aux1_1,0);
        asc2 = _mm256_insertf128_ps(auxBuffmin2,aux2_0,1);
        desc2 = _mm256_insertf128_ps(auxBuffmax2,aux2_1,0);
        asc3 = _mm256_insertf128_ps(auxBuffmin3,aux3_0,1);
        desc3 = _mm256_insertf128_ps(auxBuffmax3,aux3_1,0);
        asc4 = _mm256_insertf128_ps(auxBuffmin4,aux4_0,1);
        desc4 = _mm256_insertf128_ps(auxBuffmax4,aux4_1,0);

        auxBuffmin0 = _mm256_min_ps(asc0, desc0);
        auxBuffmax0 = _mm256_max_ps(asc0, desc0);
        auxBuffmin1 = _mm256_min_ps(asc1, desc1);
        auxBuffmax1 = _mm256_max_ps(asc1, desc1);
        auxBuffmin2 = _mm256_min_ps(asc2, desc2);
        auxBuffmax2 = _mm256_max_ps(asc2, desc2);
        auxBuffmin3 = _mm256_min_ps(asc3, desc3);
        auxBuffmax3 = _mm256_max_ps(asc3, desc3);
        auxBuffmin4 = _mm256_min_ps(asc4, desc4);
        auxBuffmax4 = _mm256_max_ps(asc4, desc4);
        
        //Third lvl ordering (4 bitonic sequences of 4 floats)
        asc0 = _mm256_shuffle_ps(auxBuffmin0, auxBuffmax0, _MM_SHUFFLE(1,0,1,0));
        desc0 = _mm256_shuffle_ps(auxBuffmin0, auxBuffmax0,_MM_SHUFFLE(3,2,3,2));
        asc1 = _mm256_shuffle_ps(auxBuffmin1, auxBuffmax1, _MM_SHUFFLE(1,0,1,0));
        desc1 = _mm256_shuffle_ps(auxBuffmin1, auxBuffmax1,_MM_SHUFFLE(3,2,3,2));
        asc2 = _mm256_shuffle_ps(auxBuffmin2, auxBuffmax2, _MM_SHUFFLE(1,0,1,0));
        desc2 = _mm256_shuffle_ps(auxBuffmin2, auxBuffmax2,_MM_SHUFFLE(3,2,3,2));
        asc3 = _mm256_shuffle_ps(auxBuffmin3, auxBuffmax3, _MM_SHUFFLE(1,0,1,0));
        desc3 = _mm256_shuffle_ps(auxBuffmin3, auxBuffmax3,_MM_SHUFFLE(3,2,3,2));
        asc4 = _mm256_shuffle_ps(auxBuffmin4, auxBuffmax4, _MM_SHUFFLE(1,0,1,0));
        desc4 = _mm256_shuffle_ps(auxBuffmin4, auxBuffmax4,_MM_SHUFFLE(3,2,3,2));

        auxBuffmin0 = _mm256_min_ps(asc0, desc0);
        auxBuffmax0 = _mm256_max_ps(asc0, desc0);
        auxBuffmin1 = _mm256_min_ps(asc1, desc1);
        auxBuffmax1 = _mm256_max_ps(asc1, desc1);
        auxBuffmin2 = _mm256_min_ps(asc2, desc2);
        auxBuffmax2 = _mm256_max_ps(asc2, desc2);
        auxBuffmin3 = _mm256_min_ps(asc3, desc3);
        auxBuffmax3 = _mm256_max_ps(asc3, desc3);
        auxBuffmin4 = _mm256_min_ps(asc4, desc4);
        auxBuffmax4 = _mm256_max_ps(asc4, desc4);
        
        //Fourth lvl ordering (8 bitonic sequences of 2 floats)
        asc0 = _mm256_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(3,1,2,0));
        desc0 = _mm256_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(2,0,3,1));
        asc1 = _mm256_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(3,1,2,0));
        desc1 = _mm256_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(2,0,3,1));
        asc2 = _mm256_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(3,1,2,0));
        desc2 = _mm256_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(2,0,3,1));
        asc3 = _mm256_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(3,1,2,0));
        desc3 = _mm256_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(2,0,3,1));
        asc4 = _mm256_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(3,1,2,0));
        desc4 = _mm256_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(2,0,3,1));

        auxBuffmin0 = _mm256_min_ps(asc0, desc0);
        auxBuffmax0 = _mm256_max_ps(asc0, desc0);
        auxBuffmin1 = _mm256_min_ps(asc1, desc1);
        auxBuffmax1 = _mm256_max_ps(asc1, desc1);
        auxBuffmin2 = _mm256_min_ps(asc2, desc2);
        auxBuffmax2 = _mm256_max_ps(asc2, desc2);
        auxBuffmin3 = _mm256_min_ps(asc3, desc3);
        auxBuffmax3 = _mm256_max_ps(asc3, desc3);
        auxBuffmin4 = _mm256_min_ps(asc4, desc4);
        auxBuffmax4 = _mm256_max_ps(asc4, desc4);
        
        //Finally,rearrange the buffers to store the ordered array
        asc0 = _mm256_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(2,0,2,0));
        desc0 = _mm256_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(3,1,3,1));
        asc1 = _mm256_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(2,0,2,0));
        desc1 = _mm256_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(3,1,3,1)); 
        asc2 = _mm256_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(2,0,2,0));
        desc2 = _mm256_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(3,1,3,1));    
        asc3 = _mm256_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(2,0,2,0));
        desc3 = _mm256_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(3,1,3,1)); 
        asc4 = _mm256_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(2,0,2,0));
        desc4 = _mm256_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(3,1,3,1));  
  
        asc0 = _mm256_shuffle_ps(asc0,asc0,_MM_SHUFFLE(3,1,2,0));
        desc0 = _mm256_shuffle_ps(desc0,desc0,_MM_SHUFFLE(3,1,2,0));
        asc1 = _mm256_shuffle_ps(asc1,asc1,_MM_SHUFFLE(3,1,2,0));
        desc1 = _mm256_shuffle_ps(desc1,desc1,_MM_SHUFFLE(3,1,2,0));
        asc2 = _mm256_shuffle_ps(asc2,asc2,_MM_SHUFFLE(3,1,2,0));
        desc2 = _mm256_shuffle_ps(desc2,desc2,_MM_SHUFFLE(3,1,2,0));
        asc3 = _mm256_shuffle_ps(asc3,asc3,_MM_SHUFFLE(3,1,2,0));
        desc3 = _mm256_shuffle_ps(desc3,desc3,_MM_SHUFFLE(3,1,2,0));
        asc4 = _mm256_shuffle_ps(asc4,asc4,_MM_SHUFFLE(3,1,2,0));
        desc4 = _mm256_shuffle_ps(desc4,desc4,_MM_SHUFFLE(3,1,2,0));

        aux0_1 = _mm256_extractf128_ps(asc0, 1);
        aux0_0 = _mm256_extractf128_ps(desc0, 0);
        aux1_1 = _mm256_extractf128_ps(asc1, 1);
        aux1_0 = _mm256_extractf128_ps(desc1, 0);
        aux2_1 = _mm256_extractf128_ps(asc2, 1);
        aux2_0 = _mm256_extractf128_ps(desc2, 0);
        aux3_1 = _mm256_extractf128_ps(asc3, 1);
        aux3_0 = _mm256_extractf128_ps(desc3, 0);
        aux4_1 = _mm256_extractf128_ps(asc4, 1);
        aux4_0 = _mm256_extractf128_ps(desc4, 0);

        desc0 = _mm256_insertf128_ps(desc0,aux0_1,0);
        asc0 = _mm256_insertf128_ps(asc0,aux0_0,1);
        desc1 = _mm256_insertf128_ps(desc1,aux1_1,0);
        asc1 = _mm256_insertf128_ps(asc1,aux1_0,1);
        desc2 = _mm256_insertf128_ps(desc2,aux2_1,0);
        asc2 = _mm256_insertf128_ps(asc2,aux2_0,1);
        desc3 = _mm256_insertf128_ps(desc3,aux3_1,0);
        asc3 = _mm256_insertf128_ps(asc3,aux3_0,1);
        desc4 = _mm256_insertf128_ps(desc4,aux4_1,0);
        asc4 = _mm256_insertf128_ps(asc4,aux4_0,1);

        _mm256_store_ps(tmp+iRight0, desc0);
        _mm256_store_ps(tmp+iLeft0, asc0);
        _mm256_store_ps(tmp+iRight1, desc1);
        _mm256_store_ps(tmp+iLeft1, asc1);
        _mm256_store_ps(tmp+iRight2, desc2);
        _mm256_store_ps(tmp+iLeft2, asc2);
        _mm256_store_ps(tmp+iRight3, desc3);
        _mm256_store_ps(tmp+iLeft3, asc3);
        _mm256_store_ps(tmp+iRight4, desc4);
        _mm256_store_ps(tmp+iLeft4, asc4);


    }else if((iEnd0-iLeft0)==8){ //Blocks have the same size so it is not required to chech iEnd1-iLeft1
        __m128 auxBuffmin0, auxBuffmax0, desc0, asc0, auxBuffmin1, auxBuffmax1, desc1,asc1, auxBuffmin2, auxBuffmax2, desc2,asc2, auxBuffmin3, auxBuffmax3, desc3,asc3, auxBuffmin4, auxBuffmax4, desc4,asc4,auxBuffmin5, auxBuffmax5, desc5,asc5;
        asc0 = _mm_load_ps(vec+iLeft0);
        desc0 = _mm_load_ps(vec+iRight0); 
        asc1 = _mm_load_ps(vec+iLeft1);
        desc1 = _mm_load_ps(vec+iRight1);
        asc2 = _mm_load_ps(vec+iLeft2);
        desc2 = _mm_load_ps(vec+iRight2);
        asc3 = _mm_load_ps(vec+iLeft3);
        desc3 = _mm_load_ps(vec+iRight3);
        asc4 = _mm_load_ps(vec+iLeft4);
        desc4 = _mm_load_ps(vec+iRight4);
        asc5 = _mm_load_ps(vec+iLeft5);
        desc5 = _mm_load_ps(vec+iRight5);
        desc0 = _mm_shuffle_ps(desc0, desc0, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc1 = _mm_shuffle_ps(desc1, desc1, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc2 = _mm_shuffle_ps(desc2, desc2, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc3 = _mm_shuffle_ps(desc3, desc3, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc4 = _mm_shuffle_ps(desc4, desc4, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence
        desc5 = _mm_shuffle_ps(desc5, desc5, _MM_SHUFFLE(0,1,2,3)); //Second half of the buffer must be in the descending order in order to create a bitonic sequence

        auxBuffmin0 = _mm_min_ps(asc0, desc0);
        auxBuffmax0 = _mm_max_ps(asc0, desc0);
        auxBuffmin1 = _mm_min_ps(asc1, desc1);
        auxBuffmax1 = _mm_max_ps(asc1, desc1);
        auxBuffmin2 = _mm_min_ps(asc2, desc2);
        auxBuffmax2 = _mm_max_ps(asc2, desc2);
        auxBuffmin3 = _mm_min_ps(asc3, desc3);
        auxBuffmax3 = _mm_max_ps(asc3, desc3);        
        auxBuffmin4 = _mm_min_ps(asc4, desc4);
        auxBuffmax4 = _mm_max_ps(asc4, desc4); 
        auxBuffmin5 = _mm_min_ps(asc5, desc5);
        auxBuffmax5 = _mm_max_ps(asc5, desc5);   

        //Second lvl ordering (2 bitonic sequences of 4 floats)
        asc0 = _mm_shuffle_ps(auxBuffmin0, auxBuffmax0, _MM_SHUFFLE(1,0,1,0));
        desc0 = _mm_shuffle_ps(auxBuffmin0, auxBuffmax0, _MM_SHUFFLE(3,2,3,2));
        asc1 = _mm_shuffle_ps(auxBuffmin1, auxBuffmax1, _MM_SHUFFLE(1,0,1,0));
        desc1 = _mm_shuffle_ps(auxBuffmin1, auxBuffmax1, _MM_SHUFFLE(3,2,3,2));
        asc2 = _mm_shuffle_ps(auxBuffmin2, auxBuffmax2, _MM_SHUFFLE(1,0,1,0));
        desc2 = _mm_shuffle_ps(auxBuffmin2, auxBuffmax2, _MM_SHUFFLE(3,2,3,2));
        asc3 = _mm_shuffle_ps(auxBuffmin3, auxBuffmax3, _MM_SHUFFLE(1,0,1,0));
        desc3 = _mm_shuffle_ps(auxBuffmin3, auxBuffmax3, _MM_SHUFFLE(3,2,3,2));
        asc4 = _mm_shuffle_ps(auxBuffmin4, auxBuffmax4, _MM_SHUFFLE(1,0,1,0));
        desc4 = _mm_shuffle_ps(auxBuffmin4, auxBuffmax4, _MM_SHUFFLE(3,2,3,2));
        asc5 = _mm_shuffle_ps(auxBuffmin5, auxBuffmax5, _MM_SHUFFLE(1,0,1,0));
        desc5 = _mm_shuffle_ps(auxBuffmin5, auxBuffmax5, _MM_SHUFFLE(3,2,3,2));
        auxBuffmin0 = _mm_min_ps(asc0, desc0);
        auxBuffmax0 = _mm_max_ps(asc0, desc0);
        auxBuffmin1 = _mm_min_ps(asc1, desc1);
        auxBuffmax1 = _mm_max_ps(asc1, desc1);
        auxBuffmin2 = _mm_min_ps(asc2, desc2);
        auxBuffmax2 = _mm_max_ps(asc2, desc2);
        auxBuffmin3 = _mm_min_ps(asc3, desc3);
        auxBuffmax3 = _mm_max_ps(asc3, desc3);
        auxBuffmin4 = _mm_min_ps(asc4, desc4);
        auxBuffmax4 = _mm_max_ps(asc4, desc4);
        auxBuffmin5 = _mm_min_ps(asc5, desc5);
        auxBuffmax5 = _mm_max_ps(asc5, desc5);
        
        //Third lvl ordering (4 bitonic sequences of 2 floats)
        asc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(3,1,2,0));
        desc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(2,0,3,1));
        asc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(3,1,2,0));
        desc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(2,0,3,1));
        asc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(3,1,2,0));
        desc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(2,0,3,1));
        asc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(3,1,2,0));
        desc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(2,0,3,1));
        asc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(3,1,2,0));
        desc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(2,0,3,1));
        asc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(3,1,2,0));
        desc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(2,0,3,1));
        auxBuffmin0 = _mm_min_ps(asc0, desc0);
        auxBuffmax0 = _mm_max_ps(asc0,desc0);
        auxBuffmin1 = _mm_min_ps(asc1, desc1);
        auxBuffmax1 = _mm_max_ps(asc1,desc1);
        auxBuffmin2 = _mm_min_ps(asc2, desc2);
        auxBuffmax2 = _mm_max_ps(asc2,desc2);
        auxBuffmin3 = _mm_min_ps(asc3, desc3);
        auxBuffmax3 = _mm_max_ps(asc3,desc3);
        auxBuffmin4 = _mm_min_ps(asc4, desc4);
        auxBuffmax4 = _mm_max_ps(asc4,desc4);
        auxBuffmin5 = _mm_min_ps(asc5, desc5);
        auxBuffmax5 = _mm_max_ps(asc5,desc5);
        
        //Finally,reorder buffers to store the ordered array
        asc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(2,0,2,0));
        desc0 = _mm_shuffle_ps(auxBuffmin0,auxBuffmax0,_MM_SHUFFLE(3,1,3,1));  
        asc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(2,0,2,0));
        desc1 = _mm_shuffle_ps(auxBuffmin1,auxBuffmax1,_MM_SHUFFLE(3,1,3,1));
        asc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(2,0,2,0));
        desc2 = _mm_shuffle_ps(auxBuffmin2,auxBuffmax2,_MM_SHUFFLE(3,1,3,1));  
        asc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(2,0,2,0));
        desc3 = _mm_shuffle_ps(auxBuffmin3,auxBuffmax3,_MM_SHUFFLE(3,1,3,1));    
        asc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(2,0,2,0));
        desc4 = _mm_shuffle_ps(auxBuffmin4,auxBuffmax4,_MM_SHUFFLE(3,1,3,1));   
        asc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(2,0,2,0));
        desc5 = _mm_shuffle_ps(auxBuffmin5,auxBuffmax5,_MM_SHUFFLE(3,1,3,1)); 
        asc0 = _mm_shuffle_ps(asc0,asc0,_MM_SHUFFLE(3,1,2,0));
        desc0 = _mm_shuffle_ps(desc0,desc0,_MM_SHUFFLE(3,1,2,0));
        asc1 = _mm_shuffle_ps(asc1,asc1,_MM_SHUFFLE(3,1,2,0));
        desc1 = _mm_shuffle_ps(desc1,desc1,_MM_SHUFFLE(3,1,2,0));
        asc2 = _mm_shuffle_ps(asc2,asc2,_MM_SHUFFLE(3,1,2,0));
        desc2 = _mm_shuffle_ps(desc2,desc2,_MM_SHUFFLE(3,1,2,0));
        asc3 = _mm_shuffle_ps(asc3,asc3,_MM_SHUFFLE(3,1,2,0));
        desc3 = _mm_shuffle_ps(desc3,desc3,_MM_SHUFFLE(3,1,2,0));
        asc4 = _mm_shuffle_ps(asc4,asc4,_MM_SHUFFLE(3,1,2,0));
        desc4 = _mm_shuffle_ps(desc4,desc4,_MM_SHUFFLE(3,1,2,0));
        asc5 = _mm_shuffle_ps(asc5,asc5,_MM_SHUFFLE(3,1,2,0));
        desc5 = _mm_shuffle_ps(desc5,desc5,_MM_SHUFFLE(3,1,2,0));

        _mm_store_ps(tmp+iLeft0, asc0);
        _mm_store_ps(tmp+iRight0, desc0);
        _mm_store_ps(tmp+iLeft1, asc1);
        _mm_store_ps(tmp+iRight1, desc1);
        _mm_store_ps(tmp+iLeft2, asc2);
        _mm_store_ps(tmp+iRight2, desc2);
        _mm_store_ps(tmp+iLeft3, asc3);
        _mm_store_ps(tmp+iRight3, desc3);
        _mm_store_ps(tmp+iLeft4, asc4);
        _mm_store_ps(tmp+iRight4, desc4);
        _mm_store_ps(tmp+iLeft5, asc5);
        _mm_store_ps(tmp+iRight5, desc5);       
    }else{
       Vector_Merge(vec, iLeft0, iRight0, iEnd0, tmp);
       Vector_Merge(vec, iLeft1, iRight1, iEnd1, tmp);
       Vector_Merge(vec, iLeft2, iRight2, iEnd2, tmp);
       Vector_Merge(vec, iLeft3, iRight3, iEnd3, tmp);
       Vector_Merge(vec, iLeft4, iRight4, iEnd4, tmp);
       Vector_Merge(vec, iLeft5, iRight5, iEnd5, tmp);


    }
}
/** Parallel Merge Sort bitonic16. This function uses the merge function Vector_bitonic16_MergeSort**/
void Vector_bitonic16_Merge(float vec[], int vecSize, float* tmp){
    int width;
    float * aux;

  /* Each 1-element run in vec[] is already "sorted". */
 
  /* Make successively longer sorted runs of length 2, 4, 8, 16... until whole array is sorted. */
    for (width = 1; width < vecSize; width = 2 * width){
        int i;
        for (i = 0; i < vecSize-10*width; i = i + 12 * width){
             /* Merge two runs: vec[i:i+width-1] and vec[i+width:i+2*width-1] to tmp[] */
             /* or copy vec[i:n-1] to tmp[] ( if(i+width >= n) ) */
             Vector_bitonic16_MergeSort(vec, i, min(i+width, vecSize), min(i+2*width, vecSize),
					     i+2*width, min(i+3*width, vecSize), min(i+4*width, vecSize),
					     i+4*width, min(i+5*width, vecSize), min(i+6*width, vecSize),
					     i+6*width, min(i+7*width, vecSize), min(i+8*width, vecSize),
					     i+8*width, min(i+9*width, vecSize), min(i+10*width, vecSize),
					     i+10*width, min(i+11*width, vecSize), min(i+12*width, vecSize), tmp);

        }
	 for (; i < vecSize; i = i + 2 * width){
             /* Merge two runs: vec[i:i+width-1] and vec[i+width:i+2*width-1] to tmp[] */
             /* or copy vec[i:n-1] to tmp[] ( if(i+width >= n) ) */
             Vector_Merge(vec, i, min(i+width, vecSize), min(i+2*width, vecSize), tmp);
        }


 
      /* Now work array tmp[] is full of runs of length 2*width. */
      /* Copy array tmp[] to array vec[] for next iteration. */

        aux=vec;
        vec=tmp;
        tmp = aux;
       /* Now array vec is full of runs of length 2*width. */
    }
}
/** Bubble Sort O(n^2) **/
void BubbleSort(float vec[], int vecSize){

    int swapped=1;
    int i;
    float temp;
    while(swapped==1){    
        swapped = 0;
        for(i=1;i<vecSize;i++){
            if(vec[i-1] > vec[i]){
                temp = vec[i];
                vec[i] = vec[i-1];
                vec[i-1] = temp;
                swapped = 1;
            }
        }
    }
}
/** Merge Sort with vector copying optimized **/
void MergeSort(float vec[], int vecSize, float* tmp){
    int width;
    float *aux;
  /* Each 1-element run in vec[] is already "sorted". */
 
  /* Make successively longer sorted runs of length 2, 4, 8, 16... until whole array is sorted. */
    for (width = 1; width < vecSize; width = 2 * width){
        int i;
 
        for (i = 0; i < vecSize; i = i + 2 * width){
             /* Merge two runs: vec[i:i+width-1] and vec[i+width:i+2*width-1] to tmp[] */
             /* or copy vec[i:n-1] to tmp[] ( if(i+width >= n) ) */
             Merge(vec, i, min(i+width, vecSize), min(i+2*width, vecSize), tmp);
        }
 
      /* Now work array tmp[] is full of runs of length 2*width. */
      /* Copy array tmp[] to array vec[] for next iteration. */
        aux=vec;
        vec=tmp;
        tmp=aux;

    }
}
/** Merge Function used by Merge Sort with vector copy optimized **/
void Merge(float vec[], int iLeft0, int iRight0, int iEnd0, float tmp[]){
    int m = iLeft0;
    int k = iRight0;
    int j;
 
  /* While there are elements in the left or right lists */
    for (j = iLeft0; j < iEnd0; j++){
      /* If left list head exists and is <= existing right list head */
        if (m < iRight0 && (k >= iEnd0 || vec[m] <= vec[k])){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
    }
}
/** Original Merge Sort**/
void OriginalMergeSort(float vec[], int vecSize, float* tmp){
    int width;
  /* Each 1-element run in vec[] is already "sorted". */
 
  /* Make successively longer sorted runs of length 2, 4, 8, 16... until whole array is sorted. */
    for (width = 1; width < vecSize; width = 2 * width){
        int i;
 
        for (i = 0; i < vecSize; i = i + 2 * width){
             /* Merge two runs: vec[i:i+width-1] and vec[i+width:i+2*width-1] to tmp[] */
             /* or copy vec[i:n-1] to tmp[] ( if(i+width >= n) ) */
             OriginalMerge(vec, i, min(i+width, vecSize), min(i+2*width, vecSize), tmp);
        }
 
      /* Now work array tmp[] is full of runs of length 2*width. */
      /* Copy array tmp[] to array vec[] for next iteration. */
        for (i = 0; i < vecSize; ++i) {
            vec[i] = tmp[i];
        }

    }
}
/** Original Merge Function used by the original Merge Sort **/
void OriginalMerge(float vec[], int iLeft0, int iRight0, int iEnd0, float tmp[]){
    int m = iLeft0;
    int k = iRight0;
    int j;
 
  /* While there are elements in the left or right lists */
    for (j = iLeft0; j < iEnd0; j++){
      /* If left list head exists and is <= existing right list head */
        if (m < iRight0 && (k >= iEnd0 || vec[m] <= vec[k])){
            tmp[j] = vec[m];
            m++;
        }else{
            tmp[j] = vec[k];
            k++;
        }
    }
}